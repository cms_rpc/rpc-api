@Register ({
	"name": "chambers::getAll",

	"fields": {
		"NAME": "string",
		"REGION": "string",
		"WHEEL_DISK": "integer",
		"RING_STATION": "integer",
		"SECTOR": "integer",
		"POSITION": "string"
	}
})

@Template
SELECT
	COMPONENTS."NAME",
	RPC_CHAMBERS.REGION,
	RPC_CHAMBERS.WHEEL_DISK,
	RPC_CHAMBERS.RING_STATION,
	RPC_CHAMBERS.SECTOR,
	RPC_CHAMBERS."POSITION"
FROM
	{% components %} COMPONENTS
	INNER JOIN
	{% rpc_chambers %} RPC_CHAMBERS
	ON COMPONENTS.UUID=RPC_CHAMBERS.UUID
ORDER BY COMPONENTS."NAME"