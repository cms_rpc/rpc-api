@Register ({
	"name": "components::getAll",

	"fields": {
		"NAME": "string",
		"DESCRIPTION": "string"
	},

	"parameters": {
		"TYPE": "string"
	}
})

@Template
SELECT "NAME", DESCRIPTION FROM
	{% components %}
WHERE "TYPE"=:TYPE
ORDER BY "NAME"