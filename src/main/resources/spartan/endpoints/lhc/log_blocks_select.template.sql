@Register ({
	"name": "lhc::selectLogBlocks",

	"fields": {
		"STARTTIME": "datetime",
		"STOPTIME": "datetime",
		"START_REASON": "string",
		"STOP_REASON": "string",
		"MACHINE_MODE": "string",
		"TYPE": "string"
	},

	"parameters": {
		"FROM": "datetime",
		"TO": "datetime"
	}
})

@Template
SELECT * FROM
	{% lhclog_blocks %}
WHERE STARTTIME BETWEEN :FROM AND :TO
ORDER BY STARTTIME