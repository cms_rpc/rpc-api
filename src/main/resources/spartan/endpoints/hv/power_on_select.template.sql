@Register ({
	"name": "hv::selectPowerOn",

	"fields": {
		"CHANGE_DATE": "datetime",
		"ON": "boolean"
	},

	"parameters": {
		"DPID": "integer",
		"FROM": "datetime",
		"TO": "datetime"
	}
})

@Template
SELECT
	CHANGE_DATE,
	"ON"
FROM
	{% rpc_active_dpid %}
WHERE DPID=:DPID AND CHANGE_DATE BETWEEN :FROM AND :TO