@Register ({
	"name": "hv::selectLumiFits",

	"fields": {
		"STARTTIME": "datetime",
		"STOPTIME": "datetime",
		"LHCFILL": "integer",
		"AVG_VMON": "double",
		"STDDEV_VMON": "double",
		"MAX_INSTLUMI": "double",
		"MIN_INSTLUMI": "double",
		"P0": "double",
		"P0_ERR": "double",
		"P1": "double",
		"P1_ERR": "double",
		"N": "integer"
	},

	"parameters": {
		"DPID": "integer",
		"FROM": "datetime",
		"TO": "datetime"
	}
})

@Template
SELECT
	RUNTIME_SUMMARY.BEGINTIME STARTTIME,
	RUNTIME_SUMMARY.ENDTIME STOPTIME,
	LUMI_FITS.LHCFILL,
	LUMI_FITS.AVG_VMON,
	LUMI_FITS.STDDEV_VMON,
	LUMI_FITS.MIN_INSTLUMI,
	LUMI_FITS.MAX_INSTLUMI,
	LUMI_FITS.P0,
	LUMI_FITS.P0_ERR,
	LUMI_FITS.P1,
	LUMI_FITS.P1_ERR,
	LUMI_FITS.N
FROM
	{% rpc_lumi_fit %} LUMI_FITS
	INNER JOIN
	{% runtime_summary %} RUNTIME_SUMMARY
	ON RUNTIME_SUMMARY.LHCFILL=LUMI_FITS.LHCFILL
WHERE LUMI_FITS.DPID=:DPID AND RUNTIME_SUMMARY.BEGINTIME BETWEEN :FROM AND :TO
AND LUMI_FITS.SIGNIFICANCE < 1E-12 AND RUNTIME_SUMMARY.BEGINTIME IS NOT NULL AND RUNTIME_SUMMARY.ENDTIME IS NOT NULL
ORDER BY RUNTIME_SUMMARY.BEGINTIME
