@Register ({
	"name": "hv::selectHVCondFits",

	"fields": {
		"STARTTIME": "datetime",
		"STOPTIME": "datetime",
		"P0": "double",
		"P0_ERR": "double",
		"P1": "double",
		"P1_ERR": "double",
		"N": "integer"
	},

	"parameters": {
		"DPID": "integer",
		"FROM": "datetime",
		"TO": "datetime"
	}
})

@Template
SELECT
	STARTTIME,
	STOPTIME,
	P0,
	P0_ERR,
	P1,
	P1_ERR,
	N
FROM
	{% rpc_hvcond_fit %} HVCOND_FITS
WHERE DPID=:DPID AND STARTTIME BETWEEN :FROM AND :TO
ORDER BY STARTTIME
