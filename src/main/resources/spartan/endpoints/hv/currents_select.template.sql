@Register ({
	"name": "hv::selectCurrents",

	"fields": {
		"CHANGE_DATE": "datetime",
		"VMON": "integer",
		"IMON": "double",
		"STATUS": "integer",
		"FLAG": "integer"
	},

	"parameters": {
		"DPID": "integer",
		"FROM": "datetime",
		"TO": "datetime"
	}
})

@Template
SELECT
	CHANGE_DATE,
	VMON,
	IMON,
	STATUS,
	FLAG
FROM
	{% dynamic_table %} RPCCURRENTS
WHERE DPID=:DPID AND CHANGE_DATE BETWEEN :FROM AND :TO