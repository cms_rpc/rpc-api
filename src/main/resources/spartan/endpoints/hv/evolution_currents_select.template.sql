@Register ({
	"name": "hv::selectEvolutionCurrents",

	"fields": {
		"STARTTIME": "datetime",
		"STOPTIME": "datetime",
		"IMON": "double",
		"IMON_ERR": "double"
	},

	"parameters": {
		"DPID": "integer",
		"FROM": "datetime",
		"TO": "datetime",
		"TYPE": "string"
	}
})

@Template
SELECT
	STARTTIME,
	STOPTIME,
	IMON,
	IMON_ERR
FROM
	{% rpccurrents_evolution %} EVO
WHERE "TYPE"=:TYPE AND DPID=:DPID AND STARTTIME BETWEEN :FROM AND :TO
ORDER BY STARTTIME