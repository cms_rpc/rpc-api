@Register ({
	"name": "hv::lastmlnotifications",

	"fields": {
		"CHAMBER_ID": "integer",
		"MODEL_ID": "integer",
		"NOTIFICATION_TYPE": "string",
		"NOTIFICATION_TIME": "datetime",
        	"AVG_DISCREPANCY": "float"
	}
})

@Template
SELECT * FROM
(SELECT
	CHAMBER_ID,
	MODEL_ID,
	NOTIFICATION_TYPE,
	NOTIFICATION_TIME,
	AVG_DISCREPANCY
FROM
	{% ml_notifications %} 
)
WHERE ROWNUM <= 20
