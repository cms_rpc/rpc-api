@Register ({
	"name": "hv::selectHVCondBlocks",

	"fields": {
		"SET_STARTTIME": "datetime",
		"STARTTIME": "datetime",
		"STOPTIME": "datetime",
		"VMON": "integer",
		"IMON": "double",
		"IMON_ERR": "double"
	},

	"parameters": {
		"DPID": "integer",
		"FROM": "datetime",
		"TO": "datetime"
	}
})

@Template
SELECT
	"SETS".STARTTIME "SET_STARTTIME",
	BLOCKS.STARTTIME,
	BLOCKS.STOPTIME,
	BLOCKS.VMON,
	BLOCKS.IMON,
	BLOCKS.IMON_ERR
FROM
	{% rpccurrent_blocks %} BLOCKS
	INNER JOIN
	{% rpccurrent_sets %} "SETS"
	ON "SETS".UUID=BLOCKS."SET"
WHERE "SETS"."TYPE"='HVCOND' AND "SETS".DPID=:DPID AND "SETS".STARTTIME BETWEEN :FROM AND :TO
ORDER BY "SETS".STARTTIME, BLOCKS.STARTTIME