@Register ({
	"name": "hv::selectIntegratedCharge",

	"fields": {
		"FROM": "datetime",
		"TO": "datetime",
		"INTEGRATED_TIME": "long",
		"INTEGRATED_CHARGE": "double"
	},

	"parameters": {
		"DPID": "string",
		"FROM": "datetime",
		"TO": "datetime",
		"IC": "string"
	}
})

@Template
SELECT
	"FROM",
	"TO",
	INTEGRATED_TIME,
	INTEGRATED_CHARGE
FROM
	{% rpc_integrated_charge %}
WHERE DPID=:DPID AND "FROM" BETWEEN :FROM AND :TO AND "TYPE"=:IC
ORDER BY "FROM"
