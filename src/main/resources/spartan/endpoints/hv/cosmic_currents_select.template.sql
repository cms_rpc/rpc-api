@Register ({
	"name": "hv::selectCosmicCurrents",

	"fields": {
		"STARTTIME": "datetime",
		"STOPTIME": "datetime",
		"IMON": "double",
		"IMON_ERR": "double"
	},

	"parameters": {
		"DPID": "integer",
		"FROM": "datetime",
		"TO": "datetime"
	}
})

@Template
SELECT
	EVO.STARTTIME,
	EVO.STOPTIME,
	EVO.IMON,
	EVO.IMON_ERR
FROM
	{% rpccurrents_evolution %} EVO
	INNER JOIN
	{% lhclog_blocks %} GAPS
	ON GAPS."TYPE"='COSMIC' AND EVO.STARTTIME BETWEEN GAPS.STARTTIME AND GAPS.STOPTIME
WHERE EVO."TYPE"='COSMIC' AND EVO.DPID=:DPID AND EVO.STARTTIME BETWEEN :FROM AND :TO
ORDER BY EVO.STARTTIME