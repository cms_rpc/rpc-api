/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.cern.cms.rpc.restapi.endpoints.components;

import ch.cern.cms.rpc.restapi.commons.components.ComponentType;
import ch.cern.cms.spartan.api.APIMessage;
import static ch.cern.cms.spartan.core.SpartanLang.input;
import io.javalin.http.Context;
import java.util.Map;
import org.apache.commons.lang3.EnumUtils;

/**
 *
 * @author omiguelc
 */
public class ComponentsRequest {

    private final Parameters parameters;

    public ComponentsRequest(Context ctx) {
        APIMessage request = new APIMessage(ctx.body());

        // READ
        this.parameters = new Parameters((Map) request.payload("parameters"));
    }

    public Parameters getParameters() {
        return parameters;
    }

    public class Parameters {

        private final ComponentType type;

        public Parameters(Map parameters) {
            this.type = input(parameters.get("type"))
                    .asString()
                    .notNull()
                    .check(input -> EnumUtils.isValidEnumIgnoreCase(ComponentType.class, input), "Invalid component_type.")
                    .map(input -> EnumUtils.getEnumIgnoreCase(ComponentType.class, input))
                    .get();
        }

        public ComponentType getType() {
            return type;
        }

    }

}
