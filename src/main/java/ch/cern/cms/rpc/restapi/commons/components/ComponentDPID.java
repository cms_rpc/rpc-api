/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.cern.cms.rpc.restapi.commons.components;

import ch.cern.cms.spartan.commons.time.Period;
import java.util.Date;
import java.util.Map;

/**
 *
 * @author omiguelc
 */
public class ComponentDPID {

    private final int dpid;
    private final Date starttime;
    private final Date stoptime;

    public ComponentDPID(Map record) {
        dpid = (int) record.get("DPID");
        starttime = (Date) record.get("STARTTIME");
        stoptime = (Date) record.get("STOPTIME");
    }

    public int getDPID() {
        return dpid;
    }

    public Date getStarttime() {
        return starttime;
    }

    public Date getStoptime() {
        return stoptime;
    }

    public Period getPeriod() {
        return new Period(starttime, stoptime);
    }
}
