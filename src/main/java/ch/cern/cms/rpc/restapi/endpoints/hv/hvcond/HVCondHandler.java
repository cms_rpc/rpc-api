/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.cern.cms.rpc.restapi.endpoints.hv.hvcond;

import static ch.cern.cms.rpc.restapi.RPCRestApi.SOURCE_CONNECTION;
import ch.cern.cms.rpc.restapi.commons.components.Component;
import ch.cern.cms.rpc.restapi.commons.components.ComponentDPID;
import static ch.cern.cms.spartan.api.API.beginMessage;
import ch.cern.cms.spartan.api.APIHandler;
import ch.cern.cms.spartan.commons.database.connections.ConnectionShield;
import ch.cern.cms.spartan.commons.database.resultsets.ResultSetStreamTuple;
import ch.cern.cms.spartan.commons.threading.pool.Loan;
import ch.cern.cms.spartan.commons.time.Period;
import static ch.cern.cms.spartan.core.SpartanLang.connection;
import static ch.cern.cms.spartan.core.SpartanLang.list;
import static ch.cern.cms.spartan.core.SpartanLang.map;
import static ch.cern.cms.spartan.core.SpartanLang.statement;
import io.javalin.http.Context;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author omiguelc
 */
public class HVCondHandler extends APIHandler {

    @Override
    public void onRequest(Context ctx) {
        HVCondRequest request = new HVCondRequest(ctx);

        try {
            Map result = new HashMap();

            result.put("columns", list(
                    "SET_STARTTIME",
                    "STARTTIME",
                    "STOPTIME",
                    "VMON",
                    "IMON",
                    "IMON_ERR"
            ));

            List<List> rows = new ArrayList();

            query(ctx, request, current -> {
                rows.add(list(
                        current.getUnixSetStarttime(),
                        current.getUnixStarttime(),
                        current.getUnixStoptime(),
                        current.getVmon(),
                        current.getImon(),
                        current.getImonErr()
                ));
            });

            result.put("rows", rows);

            // RESPOND
            ctx.result(beginMessage()
                    .payload(result)
                    .end());
        } catch (SQLException ex) {
            Logger.getLogger(HVCondHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void query(Context ctx, HVCondRequest request, Consumer<HVCondBlock> consumer) throws SQLException {
        // GET DPID LIST
        List<ComponentDPID> dpid_list = new Component(
                request.getParameters().getComponentName(),
                request.getParameters().getComponentType()
        ).getHVDPID();

        // PROCESS
        for (ComponentDPID dpid : dpid_list) {
            // FIND OVERLAP
            Period overlap = dpid.getPeriod().getOverlap(request.getParameters().getPeriod());

            // QUERY
            if (overlap != null) {
                // REQUEST
                Map query_args = map(
                        "DPID", dpid.getDPID(),
                        "FROM", overlap.getStartingDate(),
                        "TO", overlap.getEndingDate()
                );

                try (Loan<ConnectionShield> loan = connection(SOURCE_CONNECTION)) {
                    // RUN STATEMENT
                    ResultSetStreamTuple<Map> stream_tuple = statement("hv::selectHVCondBlocks")
                            .queryAndStreamMaps(loan.value(), query_args);

                    // FOR EACH
                    stream_tuple.getStream()
                            .map(HVCondBlock::new)
                            .forEach(consumer);

                    // CHECK STREAM IS OK
                    stream_tuple.getExceptionPointer().throwIfPresent();
                }
            }
        }
    }

    // CLASS
    private static class HVCondBlock {

        private final Date set_starttime;
        private final Date starttime;
        private final Date stoptime;
        private final int vmon;
        private final double imon;
        private final double imon_err;

        public HVCondBlock(Map record) {
            set_starttime = (Date) record.get("SET_STARTTIME");
            starttime = (Date) record.get("STARTTIME");
            stoptime = (Date) record.get("STOPTIME");
            vmon = (int) record.get("VMON");
            imon = (double) record.get("IMON");
            imon_err = (double) record.get("IMON_ERR");
        }

        public Date getSetStarttime() {
            return set_starttime;
        }

        public Date getStarttime() {
            return starttime;
        }

        public Date getStoptime() {
            return stoptime;
        }

        public long getUnixSetStarttime() {
            return set_starttime.getTime();
        }

        public long getUnixStarttime() {
            return starttime.getTime();
        }

        public long getUnixStoptime() {
            return stoptime.getTime();
        }

        public int getVmon() {
            return vmon;
        }

        public double getImon() {
            return imon;
        }

        public double getImonErr() {
            return imon_err;
        }

    }
}
