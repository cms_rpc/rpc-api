package ch.cern.cms.rpc.restapi.endpoints.hv.currents;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Date;
import java.util.Objects;

import static ch.cern.cms.rpc.restapi.RPCRestApi.SOURCE_CONNECTION;

import static ch.cern.cms.spartan.api.API.beginMessage;
import ch.cern.cms.rpc.restapi.endpoints.hv.evolution.EvolutionHandler;
import ch.cern.cms.spartan.api.APIHandler;
import ch.cern.cms.spartan.automation.tasks.TaskException;
import ch.cern.cms.spartan.commons.database.connections.ConnectionShield;
import ch.cern.cms.spartan.commons.database.resultsets.ResultSetStreamTuple;
import ch.cern.cms.spartan.commons.database.statements.StatementShield;
import ch.cern.cms.spartan.commons.database.statements.StatementTemplate;
import ch.cern.cms.spartan.commons.threading.pool.Loan;

import static ch.cern.cms.spartan.core.SpartanLang.connection;
import static ch.cern.cms.spartan.core.SpartanLang.list;

import io.javalin.http.Context;

import static ch.cern.cms.spartan.core.SpartanLang.template;


public class MLNotificationsHandler extends APIHandler {
    
    @Override 
    public void onRequest(Context ctx) {
        
        MLNotificationsRequest request = new MLNotificationsRequest(ctx);

        try {

            Map result = new HashMap();

            result.put("columns", list(
                "CHAMBER_ID",
                "MODEL_ID",
                "NOTIFICATION_TYPE",
                "NOTIFICATION_TIME",
                "AVG_DISCREPANCY"        
            ));

            List<List> rows = new ArrayList();

            query(request, ctx , notification -> {
                rows.add(list(
                        notification.getChamberID(),
                        notification.getModelID(),
                        notification.getNotificationType(),
                        notification.getNotificationTime(),
                        notification.getAvgDiscrepancy()
                ));
            });

            result.put("rows", rows);

            // RESPOND
            ctx.result(beginMessage()
            .payload(result)
            .end());
    
        } catch (SQLException ex) {
            Logger.getLogger(EvolutionHandler.class.getName()).log(Level.SEVERE, null, ex);
        }

    } 

    private void query(MLNotificationsRequest request, Context ctx, Consumer<MLNotification> consumer) throws SQLException{


        StatementTemplate template = template("hv::lastmlnotifications");

        StatementShield statement = template.statement();

        try (Loan<ConnectionShield> loan = connection(SOURCE_CONNECTION)) {

            // RUN STATEMENT
            ResultSetStreamTuple<Map> resultSetStream = statement.queryAndStreamMaps(loan.value());
            
            // Map record = new HashMap();

            resultSetStream.getStream()
                // .forEach(new_record -> {
                // })
                .filter(Objects::nonNull)
                .map(MLNotification::new)
                .forEach(consumer);

            // CHECK STREAM IS OK
            resultSetStream.getExceptionPointer().throwIfPresent();

        } catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
        }

        
    }

    // ML Notification Class
    private static class MLNotification {

        private final int chamberID;
        private final int modelID;
        private final String notificationType;
        private final Date notificationTime;
        private final float avgDiscrepancy;


        public MLNotification(Map record) {

            this.chamberID = (int) record.get("CHAMBER_ID");
            this.modelID = (int) record.get("MODEL_ID");
            this.notificationType = (String) record.get("NOTIFICATION_TYPE");
            this.notificationTime = (Date) record.get("NOTIFICATION_TIME");
            this.avgDiscrepancy = (float) record.get("AVG_DISCREPANCY");

        }

        public int getChamberID() {
            return chamberID;
        }

        public int getModelID() {
            return chamberID;
        }

        public String getNotificationType() {
            return notificationType;
        }   

        public Date getNotificationTime() {
            return notificationTime;
        }

        public float getAvgDiscrepancy() {
            return avgDiscrepancy;
        }
    }

}