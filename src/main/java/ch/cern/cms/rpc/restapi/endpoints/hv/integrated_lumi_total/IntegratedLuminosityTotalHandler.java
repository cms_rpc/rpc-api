/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.cern.cms.rpc.restapi.endpoints.hv.integrated_lumi_total;

import static ch.cern.cms.rpc.restapi.RPCRestApi.SOURCE_CONNECTION;
import static ch.cern.cms.spartan.api.API.beginMessage;
import ch.cern.cms.spartan.api.APIHandler;
import ch.cern.cms.spartan.commons.database.connections.ConnectionShield;
import ch.cern.cms.spartan.commons.database.resultsets.ResultSetStreamTuple;
import ch.cern.cms.spartan.commons.threading.pool.Loan;
import ch.cern.cms.spartan.commons.time.Period;
import static ch.cern.cms.spartan.core.SpartanLang.connection;
import static ch.cern.cms.spartan.core.SpartanLang.map;
import static ch.cern.cms.spartan.core.SpartanLang.statement;
import io.javalin.http.Context;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author omiguelc
 */
public class IntegratedLuminosityTotalHandler extends APIHandler {

    @Override
    public void onRequest(Context ctx) {
        IntegratedLuminosityTotalRequest request = new IntegratedLuminosityTotalRequest(ctx);

        try {
            Map result = new HashMap();

            query(ctx, request, integrated_lumi -> {
                result.put("FROM", integrated_lumi.getFromInUnixtime());
                result.put("TO", integrated_lumi.getToInUnixtime());
                result.put("RECORDED", integrated_lumi.getRecorded());
                result.put("DELIVERED", integrated_lumi.getDelivered());
            });

            // RESPOND
            ctx.result(beginMessage()
                    .payload(result)
                    .end());
        } catch (SQLException ex) {
            Logger.getLogger(IntegratedLuminosityTotalHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void query(Context ctx, IntegratedLuminosityTotalRequest request, Consumer<IntegratedLumi> consumer) throws SQLException {
        Map query_args = map(
                "FROM", request.getParameters().getFrom(),
                "TO", request.getParameters().getTo()
        );

        try (Loan<ConnectionShield> loan = connection(SOURCE_CONNECTION)) {
            // INIT INTEGRATOR
            Integrator integrator = new Integrator();

            // RUN STATEMENT
            ResultSetStreamTuple<Map> stream_tuple = statement("cms::selectIntegratedLuminosity")
                    .queryAndStreamMaps(loan.value(), query_args);

            // FOR EACH
            stream_tuple.getStream()
                    .map(IntegratedLumi::new)
                    .forEach(integrator::process);

            // CHECK STREAM IS OK
            stream_tuple.getExceptionPointer().throwIfPresent();

            // DISPATCH
            consumer.accept(new IntegratedLumi(map(
                    "FROM", request.getParameters().getFrom(),
                    "TO", request.getParameters().getTo(),
                    "RECORDED", integrator.getRecorded(),
                    "DELIVERED", integrator.getDelivered()
            )));
        }
    }

    // CLASS
    private static class Integrator {

        private double recorded;
        private double delivered;

        public Integrator() {
            recorded = 0;
            delivered = 0;
        }

        public void process(IntegratedLumi lumi) {
            recorded += lumi.getRecorded();
            delivered += lumi.getDelivered();
        }

        public double getRecorded() {
            return recorded;
        }

        public double getDelivered() {
            return delivered;
        }
    }

    private static class IntegratedLumi {

        private final Date from;
        private final Date to;

        private final Period period;

        private final double recorded;
        private final double delivered;

        public IntegratedLumi(Map record) {
            this.from = (Date) record.get("FROM");
            this.to = (Date) record.get("TO");

            this.period = new Period(from, to);

            this.recorded = (double) record.get("RECORDED"); // PICO BARN
            this.delivered = (double) record.get("DELIVERED"); // PICO BARN
        }

        public Date getFrom() {
            return from;
        }

        public Date getTo() {
            return to;
        }

        public long getFromInUnixtime() {
            return from.getTime();
        }

        public long getToInUnixtime() {
            return to.getTime();
        }

        public Period getPeriod() {
            return period;
        }

        public double getRecorded() {
            return recorded;
        }

        public double getDelivered() {
            return delivered;
        }
    }
}
