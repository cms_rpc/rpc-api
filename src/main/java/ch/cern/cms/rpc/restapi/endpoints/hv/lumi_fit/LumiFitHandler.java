/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.cern.cms.rpc.restapi.endpoints.hv.lumi_fit;

import static ch.cern.cms.rpc.restapi.RPCRestApi.SOURCE_CONNECTION;
import ch.cern.cms.rpc.restapi.commons.components.Component;
import ch.cern.cms.rpc.restapi.commons.components.ComponentDPID;
import ch.cern.cms.rpc.restapi.commons.components.ComponentType;
import ch.cern.cms.rpc.restapi.commons.hv.Normalizer;
import static ch.cern.cms.spartan.api.API.beginMessage;
import ch.cern.cms.spartan.api.APIHandler;
import ch.cern.cms.spartan.commons.database.connections.ConnectionShield;
import ch.cern.cms.spartan.commons.database.resultsets.ResultSetStreamTuple;
import ch.cern.cms.spartan.commons.threading.pool.Loan;
import ch.cern.cms.spartan.commons.time.Period;
import static ch.cern.cms.spartan.core.SpartanLang.connection;
import static ch.cern.cms.spartan.core.SpartanLang.list;
import static ch.cern.cms.spartan.core.SpartanLang.map;
import static ch.cern.cms.spartan.core.SpartanLang.statement;
import io.javalin.http.Context;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author omiguelc
 */
public class LumiFitHandler extends APIHandler {

    @Override
    public void onRequest(Context ctx) {
        LumiFitRequest request = new LumiFitRequest(ctx);

        try {
            Map result = new HashMap();

            result.put("columns", list(
                    "STARTTIME",
                    "STOPTIME",
                    "LHCFILL",
                    "AVG_VMON",
                    "STDDEV_VMON",
                    "MAX_INSTLUMI",
                    "MIN_INSTLUMI",
                    "P0",
                    "P0_ERR",
                    "P1",
                    "P1_ERR",
                    "N"
            ));

            List<List> rows = new ArrayList();

            query(ctx, request, fit -> {
                rows.add(list(
                        fit.getStarttimeInUnixtime(),
                        fit.getStoptimeInUnixtime(),
                        fit.getLhcfill(),
                        fit.getAvgVmon(),
                        fit.getStddevVmon(),
                        fit.getMaxInstlumi(),
                        fit.getMinInstlumi(),
                        fit.getP0(),
                        fit.getP0Err(),
                        fit.getP1(),
                        fit.getP1Err(),
                        fit.getN()
                ));
            });

            result.put("rows", rows);

            // RESPOND
            ctx.result(beginMessage()
                    .payload(result)
                    .end());
        } catch (SQLException ex) {
            Logger.getLogger(LumiFitHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void query(Context ctx, LumiFitRequest request, Consumer<LumiFit> consumer) throws SQLException {
        // GET DPID LIST
        List<ComponentDPID> dpid_list = new Component(
                request.getParameters().getComponentName(),
                request.getParameters().getComponentType()
        ).getHVDPID();

        // PROCESS
        for (ComponentDPID dpid : dpid_list) {
            // FIND OVERLAP
            Period overlap = dpid.getPeriod().getOverlap(request.getParameters().getPeriod());

            // QUERY
            if (overlap != null) {
                // REQUEST
                Map query_args = map(
                        "DPID", dpid.getDPID(),
                        "FROM", overlap.getStartingDate(),
                        "TO", overlap.getEndingDate()
                );

                try (Loan<ConnectionShield> loan = connection(SOURCE_CONNECTION)) {
                    Normalization normalization = new Normalization();

                    // GET NORMALIZERS
                    String normalizer_source;

                    if (ComponentType.VIRTUAL_CHAMBER.equals(request.getParameters().getComponentType())) {
                        normalizer_source = "hv::selectVirtualNormalizers";
                    } else {
                        normalizer_source = "hv::selectNormalizers";
                    }

                    statement(normalizer_source)
                            .queryAndGetMaps(
                                    loan.value(),
                                    map("COMPONENT_NAME", request.getParameters().getComponentName())
                            )
                            .stream()
                            .map(Normalizer::new)
                            .forEach(normalization::addNormalizer);

                    // RUN STATEMENT
                    ResultSetStreamTuple<Map> stream_tuple = statement("hv::selectLumiFits")
                            .queryAndStreamMaps(loan.value(), query_args);

                    // FOR EACH
                    stream_tuple.getStream()
                            .map(LumiFit::new)
                            .peek(normalization::apply)
                            .forEach(consumer);

                    // CHECK STREAM IS OK
                    stream_tuple.getExceptionPointer().throwIfPresent();
                }
            }
        }
    }

    private static class Normalization {

        private Normalizer normalizer;
        private final List<Normalizer> normalizers;

        public Normalization() {
            normalizers = new ArrayList();
        }

        public void addNormalizer(Normalizer normalizer) {
            if (this.normalizer == null) {
                this.normalizer = normalizer;
            } else {
                normalizers.add(normalizer);
            }
        }

        public void apply(LumiFit record) {
            if (!normalizer.validFor(record.getStarttime())) {
                // FIND NORMALIZER
                for (Normalizer candidate : normalizers) {
                    if (candidate.validFor(record.getStarttime())) {
                        normalizer = candidate;
                        break;
                    }
                }

                // REMOVE FROM NORMALIZERS
                normalizers.remove(normalizer);
            }

            // Normalize
            if (normalizer != null) {
                record.setP0(record.getP0() / normalizer.getArea());
                record.setP0Err(record.getP0Err() / normalizer.getArea());
                record.setP1(record.getP1() / normalizer.getArea());
                record.setP1Err(record.getP1Err() / normalizer.getArea());
            }
        }

        public Normalizer getCurrentNormalizer() {
            return normalizer;
        }
    }

    private static class LumiFit {

        private final Date starttime;
        private final Date stoptime;
        private final int lhcfill;
        private final double avg_vmon;
        private final double stddev_vmon;
        private final double max_instlumi;
        private final double min_instlumi;
        private double p0;
        private double p0_err;
        private double p1;
        private double p1_err;
        private final int n;

        public LumiFit(Map record) {
            starttime = (Date) record.get("STARTTIME");
            stoptime = (Date) record.get("STOPTIME");
            lhcfill = (int) record.get("LHCFILL");
            avg_vmon = (double) record.get("AVG_VMON");
            stddev_vmon = (double) record.get("STDDEV_VMON");
            max_instlumi = (double) record.get("MAX_INSTLUMI");
            min_instlumi = (double) record.get("MIN_INSTLUMI");
            p0 = (double) record.get("P0");
            p0_err = (double) record.get("P0_ERR");
            p1 = (double) record.get("P1");
            p1_err = (double) record.get("P1_ERR");
            n = (int) record.get("N");
        }

        // GETTERS
        public Date getStarttime() {
            return starttime;
        }

        public Date getStoptime() {
            return stoptime;
        }

        public long getStarttimeInUnixtime() {
            return starttime.getTime();
        }

        public long getStoptimeInUnixtime() {
            return stoptime.getTime();
        }

        public int getLhcfill() {
            return lhcfill;
        }

        public double getAvgVmon() {
            return avg_vmon;
        }

        public double getStddevVmon() {
            return stddev_vmon;
        }

        public double getMaxInstlumi() {
            return max_instlumi;
        }

        public double getMinInstlumi() {
            return min_instlumi;
        }

        public double getP0() {
            return p0;
        }

        public double getP0Err() {
            return p0_err;
        }

        public double getP1() {
            return p1;
        }

        public double getP1Err() {
            return p1_err;
        }

        public int getN() {
            return n;
        }

        // SETTERS
        public void setP0(double p0) {
            this.p0 = p0;
        }

        public void setP0Err(double p0_err) {
            this.p0_err = p0_err;
        }

        public void setP1(double p1) {
            this.p1 = p1;
        }

        public void setP1Err(double p1_err) {
            this.p1_err = p1_err;
        }

    }
}
