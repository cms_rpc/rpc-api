/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.cern.cms.rpc.restapi.commons.components;

/**
 *
 * @author omiguelc
 */
public enum ComponentType {
    CHAMBER, VIRTUAL_CHAMBER
}
