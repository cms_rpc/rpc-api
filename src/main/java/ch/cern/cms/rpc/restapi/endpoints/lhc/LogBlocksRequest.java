/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.cern.cms.rpc.restapi.endpoints.lhc;

import ch.cern.cms.spartan.api.APIMessage;
import static ch.cern.cms.spartan.core.SpartanLang.input;
import io.javalin.http.Context;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

/**
 *
 * @author omiguelc
 */
public class LogBlocksRequest {

    private final Parameters parameters;

    public LogBlocksRequest(Context ctx) {
        APIMessage request = new APIMessage(ctx.body());

        // READ
        this.parameters = new Parameters((Map) request.payload("parameters"));
    }

    public Parameters getParameters() {
        return parameters;
    }

    public class Parameters {

        private final Date from;
        private final Date to;

        public Parameters(Map parameters) {
            this.from = input(parameters.get("from"))
                    .asDate()
                    .notNull()
                    .get();

            this.to = input(parameters.get("to"))
                    .asDate()
                    .ifNull(Calendar.getInstance().getTime())
                    .get();
        }

        public Date getFrom() {
            return from;
        }

        public Date getTo() {
            return to;
        }
    }
}
