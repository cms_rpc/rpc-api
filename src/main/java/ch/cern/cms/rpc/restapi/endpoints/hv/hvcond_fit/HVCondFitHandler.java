/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.cern.cms.rpc.restapi.endpoints.hv.hvcond_fit;

import static ch.cern.cms.rpc.restapi.RPCRestApi.SOURCE_CONNECTION;
import ch.cern.cms.rpc.restapi.commons.components.Component;
import ch.cern.cms.rpc.restapi.commons.components.ComponentDPID;
import static ch.cern.cms.spartan.api.API.beginMessage;
import ch.cern.cms.spartan.api.APIHandler;
import ch.cern.cms.spartan.commons.database.connections.ConnectionShield;
import ch.cern.cms.spartan.commons.database.resultsets.ResultSetStreamTuple;
import ch.cern.cms.spartan.commons.threading.pool.Loan;
import ch.cern.cms.spartan.commons.time.Period;
import static ch.cern.cms.spartan.core.SpartanLang.connection;
import static ch.cern.cms.spartan.core.SpartanLang.list;
import static ch.cern.cms.spartan.core.SpartanLang.map;
import static ch.cern.cms.spartan.core.SpartanLang.statement;
import io.javalin.http.Context;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author omiguelc
 */
public class HVCondFitHandler extends APIHandler {

    @Override
    public void onRequest(Context ctx) {
        HVCondFitRequest request = new HVCondFitRequest(ctx);

        try {
            Map result = new HashMap();

            result.put("columns", list(
                    "STARTTIME",
                    "STOPTIME",
                    "P0",
                    "P0_ERR",
                    "P1",
                    "P1_ERR",
                    "N"
            ));

            List<List> rows = new ArrayList();

            query(ctx, request, fit -> {
                rows.add(list(
                        fit.getStarttimeInUnixtime(),
                        fit.getStoptimeInUnixtime(),
                        fit.getP0(),
                        fit.getP0Err(),
                        fit.getP1(),
                        fit.getP1Err(),
                        fit.getN()
                ));
            });

            result.put("rows", rows);

            // RESPOND
            ctx.result(beginMessage()
                    .payload(result)
                    .end());
        } catch (SQLException ex) {
            Logger.getLogger(HVCondFitHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void query(Context ctx, HVCondFitRequest request, Consumer<HVCondFit> consumer) throws SQLException {
        // GET DPID LIST
        List<ComponentDPID> dpid_list = new Component(
                request.getParameters().getComponentName(),
                request.getParameters().getComponentType()
        ).getHVDPID();

        // PROCESS
        for (ComponentDPID dpid : dpid_list) {
            // FIND OVERLAP
            Period overlap = dpid.getPeriod().getOverlap(request.getParameters().getPeriod());

            // QUERY
            if (overlap != null) {
                // REQUEST
                Map query_args = map(
                        "DPID", dpid.getDPID(),
                        "FROM", overlap.getStartingDate(),
                        "TO", overlap.getEndingDate()
                );

                try (Loan<ConnectionShield> loan = connection(SOURCE_CONNECTION)) {
                    // RUN STATEMENT
                    ResultSetStreamTuple<Map> stream_tuple = statement("hv::selectHVCondFits")
                            .queryAndStreamMaps(loan.value(), query_args);

                    // FOR EACH
                    stream_tuple.getStream()
                            .map(HVCondFit::new)
                            .forEach(consumer);

                    // CHECK STREAM IS OK
                    stream_tuple.getExceptionPointer().throwIfPresent();
                }
            }
        }
    }

    private static class HVCondFit {

        private final Date starttime;
        private final Date stoptime;
        private final double p0;
        private final double p0_err;
        private final double p1;
        private final double p1_err;
        private final int n;

        public HVCondFit(Map record) {
            starttime = (Date) record.get("STARTTIME");
            stoptime = (Date) record.get("STOPTIME");
            p0 = (double) record.get("P0");
            p0_err = (double) record.get("P0_ERR");
            p1 = (double) record.get("P1");
            p1_err = (double) record.get("P1_ERR");
            n = (int) record.get("N");
        }

        public Date getStarttime() {
            return starttime;
        }

        public Date getStoptime() {
            return stoptime;
        }

        public long getStarttimeInUnixtime() {
            return starttime.getTime();
        }

        public long getStoptimeInUnixtime() {
            return stoptime.getTime();
        }

        public double getP0() {
            return p0;
        }

        public double getP0Err() {
            return p0_err;
        }

        public double getP1() {
            return p1;
        }

        public double getP1Err() {
            return p1_err;
        }

        public int getN() {
            return n;
        }
    }
}
