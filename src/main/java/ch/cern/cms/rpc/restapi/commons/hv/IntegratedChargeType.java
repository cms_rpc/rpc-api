/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.cern.cms.rpc.restapi.commons.hv;

/**
 *
 * @author omiguelc
 */
public enum IntegratedChargeType {
    RPC_ON, COLLISION, COLLISION_NO_COSMIC
}
