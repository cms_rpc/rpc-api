/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.cern.cms.rpc.restapi.endpoints.components.chamber;

import static ch.cern.cms.rpc.restapi.RPCRestApi.SOURCE_CONNECTION;
import static ch.cern.cms.spartan.api.API.beginMessage;
import ch.cern.cms.spartan.api.APIHandler;
import ch.cern.cms.spartan.commons.database.connections.ConnectionShield;
import ch.cern.cms.spartan.commons.database.resultsets.ResultSetStreamTuple;
import ch.cern.cms.spartan.commons.threading.pool.Loan;
import static ch.cern.cms.spartan.core.SpartanLang.connection;
import static ch.cern.cms.spartan.core.SpartanLang.list;
import static ch.cern.cms.spartan.core.SpartanLang.statement;
import io.javalin.http.Context;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author omiguelc
 */
public class ChambersHandler extends APIHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(ChambersHandler.class);

    @Override
    public void onRequest(Context ctx) {
        ChambersRequest request = new ChambersRequest(ctx);

        try {
            Map result = new HashMap();

            result.put("columns", list(
                    "NAME",
                    "REGION",
                    "WHEEL_DISK",
                    "RING_STATION",
                    "SECTOR",
                    "POSITION"
            ));

            List<List> rows = new ArrayList();

            query(ctx, request, info -> {
                rows.add(list(
                        info.getName(),
                        info.getRegion(),
                        info.getWheelDisk(),
                        info.getRingStation(),
                        info.getSector(),
                        info.getPosition()
                ));
            });

            result.put("rows", rows);

            // RESPOND
            ctx.result(beginMessage()
                    .payload(result)
                    .end());
        } catch (SQLException ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
    }

    private void query(Context ctx, ChambersRequest request, Consumer<Chamber> consumer) throws SQLException {
        try (Loan<ConnectionShield> loan = connection(SOURCE_CONNECTION)) {
            // RUN STATEMENT
            ResultSetStreamTuple<Map> stream_tuple = statement("chambers::getAll")
                    .queryAndStreamMaps(loan.value());

            // FOR EACH
            stream_tuple.getStream()
                    .map(Chamber::new)
                    .forEach(consumer);

            // CHECK STREAM IS OK
            stream_tuple.getExceptionPointer().throwIfPresent();
        }
    }

    // CLASS
    private static class Chamber {

        private final String name;
        private final String region;
        private final int wheel_disk;
        private final int ring_station;
        private final int sector;
        private final String position;

        public Chamber(Map record) {
            this.name = (String) record.get("NAME");
            this.region = (String) record.get("REGION");
            this.wheel_disk = (int) record.get("WHEEL_DISK");
            this.ring_station = (int) record.get("RING_STATION");
            this.sector = (int) record.get("SECTOR");
            this.position = (String) record.get("POSITION");
        }

        public String getName() {
            return name;
        }

        public String getRegion() {
            return region;
        }

        public int getWheelDisk() {
            return wheel_disk;
        }

        public int getRingStation() {
            return ring_station;
        }

        public int getSector() {
            return sector;
        }

        public String getPosition() {
            return position;
        }

    }
}
