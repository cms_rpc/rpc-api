/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.cern.cms.rpc.restapi.endpoints.cms.integrated_lumi;

import static ch.cern.cms.rpc.restapi.RPCRestApi.SOURCE_CONNECTION;
import static ch.cern.cms.spartan.api.API.beginMessage;
import ch.cern.cms.spartan.api.APIHandler;
import ch.cern.cms.spartan.commons.database.connections.ConnectionShield;
import ch.cern.cms.spartan.commons.database.resultsets.ResultSetStreamTuple;
import ch.cern.cms.spartan.commons.threading.pool.Loan;
import ch.cern.cms.spartan.commons.time.Period;
import static ch.cern.cms.spartan.core.SpartanLang.connection;
import static ch.cern.cms.spartan.core.SpartanLang.list;
import static ch.cern.cms.spartan.core.SpartanLang.map;
import static ch.cern.cms.spartan.core.SpartanLang.statement;
import io.javalin.http.Context;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author omiguelc
 */
public class IntegratedLuminosityHandler extends APIHandler {

    @Override
    public void onRequest(Context ctx) {
        IntegratedLuminosityRequest request = new IntegratedLuminosityRequest(ctx);

        try {
            Map result = new HashMap();

            result.put("columns", list(
                    "CHANGE_DATE",
                    "RECORDED",
                    "DELIVERED"
            ));

            List<List> rows = new ArrayList();

            query(ctx, request, lumi -> {
                rows.add(list(
                        lumi.getToInUnixtime(),
                        lumi.getRecorded(),
                        lumi.getDelivered()
                ));
            });

            result.put("rows", rows);

            // RESPOND
            ctx.result(beginMessage()
                    .payload(result)
                    .end());
        } catch (SQLException ex) {
            Logger.getLogger(IntegratedLuminosityHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void query(Context ctx, IntegratedLuminosityRequest request, Consumer<IntegratedLumi> consumer) throws SQLException {
        Map query_args = map(
                "FROM", request.getParameters().getFrom(),
                "TO", request.getParameters().getTo()
        );

        try (Loan<ConnectionShield> loan = connection(SOURCE_CONNECTION)) {
            // INIT INTEGRATOR
            Integrator integrator = new Integrator();

            // RUN STATEMENT
            ResultSetStreamTuple<Map> stream_tuple = statement("cms::selectIntegratedLuminosity")
                    .queryAndStreamMaps(loan.value(), query_args);

            // FOR EACH
            stream_tuple.getStream()
                    .map(IntegratedLumi::new)
                    .map(integrated_lumi -> {
                        integrator.process(integrated_lumi);

                        return new IntegratedLumi(map(
                                "FROM", request.getParameters().getFrom(),
                                "TO", integrated_lumi.getTo(),
                                "RECORDED", integrator.getRecorded(),
                                "DELIVERED", integrator.getDelivered()
                        ));
                    })
                    .filter(Objects::nonNull)
                    .forEach(consumer);

            // CHECK STREAM IS OK
            stream_tuple.getExceptionPointer().throwIfPresent();
        }
    }

    // CLASS
    private static class Integrator {

        private double recorded;
        private double delivered;

        public Integrator() {
            recorded = 0;
            delivered = 0;
        }

        public void process(IntegratedLumi lumi) {
            recorded += lumi.getRecorded();
            delivered += lumi.getDelivered();
        }

        public double getRecorded() {
            return recorded;
        }

        public double getDelivered() {
            return delivered;
        }
    }

    private static class IntegratedLumi {

        private final Date from;
        private final Date to;

        private final Period period;

        private final double recorded;
        private final double delivered;

        public IntegratedLumi(Map record) {
            this.from = (Date) record.get("FROM");
            this.to = (Date) record.get("TO");

            this.period = new Period(from, to);

            this.recorded = (double) record.get("RECORDED"); // PICO BARN
            this.delivered = (double) record.get("DELIVERED"); // PICO BARN
        }

        public Date getFrom() {
            return from;
        }

        public Date getTo() {
            return to;
        }

        public long getFromInUnixtime() {
            return from.getTime();
        }

        public long getToInUnixtime() {
            return to.getTime();
        }

        public Period getPeriod() {
            return period;
        }

        public double getRecorded() {
            return recorded;
        }

        public double getDelivered() {
            return delivered;
        }
    }
}
