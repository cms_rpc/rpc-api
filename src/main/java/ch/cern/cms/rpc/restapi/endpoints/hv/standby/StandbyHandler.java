/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.cern.cms.rpc.restapi.endpoints.hv.standby;

import static ch.cern.cms.rpc.restapi.RPCRestApi.SOURCE_CONNECTION;
import ch.cern.cms.rpc.restapi.commons.components.Component;
import ch.cern.cms.rpc.restapi.commons.components.ComponentDPID;
import static ch.cern.cms.spartan.api.API.beginMessage;
import ch.cern.cms.spartan.api.APIHandler;
import ch.cern.cms.spartan.commons.database.connections.ConnectionShield;
import ch.cern.cms.spartan.commons.database.resultsets.ResultSetStreamTuple;
import ch.cern.cms.spartan.commons.threading.pool.Loan;
import ch.cern.cms.spartan.commons.time.Period;
import static ch.cern.cms.spartan.core.SpartanLang.connection;
import static ch.cern.cms.spartan.core.SpartanLang.list;
import static ch.cern.cms.spartan.core.SpartanLang.map;
import static ch.cern.cms.spartan.core.SpartanLang.statement;
import io.javalin.http.Context;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author omiguelc
 */
public class StandbyHandler extends APIHandler {

    @Override
    public void onRequest(Context ctx) {
        StandbyRequest request = new StandbyRequest(ctx);

        try {
            Map result = new HashMap();

            result.put("columns", list(
                    "STARTTIME",
                    "STOPTIME",
                    "IMON",
                    "IMON_ERR"
            ));

            List<List> rows = new ArrayList();

            query(ctx, request, current -> {
                rows.add(list(
                        current.getUnixStarttime(),
                        current.getUnixStoptime(),
                        current.getImon(),
                        current.getImonErr()
                ));
            });

            result.put("rows", rows);

            // RESPOND
            ctx.result(beginMessage()
                    .payload(result)
                    .end());
        } catch (SQLException ex) {
            Logger.getLogger(StandbyHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void query(Context ctx, StandbyRequest request, Consumer<StandbyCurrent> consumer) throws SQLException {
        // GET DPID LIST
        List<ComponentDPID> dpid_list = new Component(
                request.getParameters().getComponentName(),
                request.getParameters().getComponentType()
        ).getHVDPID();

        // PROCESS
        for (ComponentDPID dpid : dpid_list) {
            // FIND OVERLAP
            Period overlap = dpid.getPeriod().getOverlap(request.getParameters().getPeriod());

            // QUERY
            if (overlap != null) {
                // REQUEST
                Map query_args = map(
                        "DPID", dpid.getDPID(),
                        "FROM", overlap.getStartingDate(),
                        "TO", overlap.getEndingDate()
                );

                try (Loan<ConnectionShield> loan = connection(SOURCE_CONNECTION)) {
                    // RUN STATEMENT
                    ResultSetStreamTuple<Map> stream_tuple = statement("hv::selectStandbyCurrents")
                            .queryAndStreamMaps(loan.value(), query_args);

                    // FOR EACH
                    stream_tuple.getStream()
                            .map(StandbyCurrent::new)
                            .forEach(consumer);

                    // CHECK STREAM IS OK
                    stream_tuple.getExceptionPointer().throwIfPresent();
                }
            }
        }
    }

    private static class StandbyCurrent {

        private final Date starttime;
        private final Date stoptime;
        private final double imon;
        private final double imon_err;

        public StandbyCurrent(Map record) {
            starttime = (Date) record.get("STARTTIME");
            stoptime = (Date) record.get("STOPTIME");
            imon = (double) record.get("IMON");
            imon_err = (double) record.get("IMON_ERR");
        }

        public Date getStarttime() {
            return starttime;
        }

        public Date getStoptime() {
            return stoptime;
        }

        public long getUnixStarttime() {
            return starttime.getTime();
        }

        public long getUnixStoptime() {
            return stoptime.getTime();
        }

        public double getImon() {
            return imon;
        }

        public double getImonErr() {
            return imon_err;
        }
    }
}
