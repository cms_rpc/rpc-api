/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.cern.cms.rpc.restapi;

import ch.cern.cms.rpc.restapi.endpoints.cms.integrated_lumi.IntegratedLuminosityHandler;
import ch.cern.cms.rpc.restapi.endpoints.cms.integrated_lumi_total.IntegratedLuminosityTotalHandler;
import ch.cern.cms.rpc.restapi.endpoints.components.ComponentsHandler;
import ch.cern.cms.rpc.restapi.endpoints.components.chamber.ChambersHandler;
import ch.cern.cms.rpc.restapi.endpoints.hv.cosmic.CosmicHandler;
import ch.cern.cms.rpc.restapi.endpoints.hv.currents.CurrentsHandler;

import ch.cern.cms.rpc.restapi.endpoints.hv.currents.MLNotificationsHandler;
import ch.cern.cms.rpc.restapi.endpoints.hv.evolution.EvolutionHandler;
import ch.cern.cms.rpc.restapi.endpoints.hv.hvcond.HVCondHandler;
import ch.cern.cms.rpc.restapi.endpoints.hv.hvcond_fit.HVCondFitHandler;
import ch.cern.cms.rpc.restapi.endpoints.hv.integrated_charge.IntegratedChargeHandler;
import ch.cern.cms.rpc.restapi.endpoints.hv.integrated_charge_per_year.IntegratedChargePerYearHandler;
import ch.cern.cms.rpc.restapi.endpoints.hv.integrated_charge_total.IntegratedChargeTotalHandler;
import ch.cern.cms.rpc.restapi.endpoints.hv.lumi_fit.LumiFitHandler;
import ch.cern.cms.rpc.restapi.endpoints.hv.power_on.PowerOnHandler;
import ch.cern.cms.rpc.restapi.endpoints.hv.standby.StandbyHandler;
import ch.cern.cms.rpc.restapi.endpoints.lhc.LogBlocksHandler;
import ch.cern.cms.spartan.api.API;
import ch.cern.cms.spartan.core.Spartan;
import static ch.cern.cms.spartan.core.SpartanLang.cget;
import static ch.cern.cms.spartan.core.SpartanLang.input;
import io.javalin.Javalin;
import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author omiguelc
 */
public class RPCRestApi {

    // MAIN
    public static void main(String[] args) {
        // ARGS
        ArgumentParser parser = ArgumentParsers.newFor("RPC REST API")
                .build()
                .defaultHelp(true)
                .description("RPC REST API");

        parser.addArgument("-d", "--home-directory")
                .metavar("Home Directory")
                .help("Home Directory which contains the app.json file and more config files if they exist.")
                .dest("home_directory")
                .type(String.class)
                .required(true);

        Namespace namespace = null;

        try {
            namespace = parser.parseArgs(args);
        } catch (ArgumentParserException ex) {
            // HANDLE
            parser.handleError(ex);
        }

        if (namespace != null) {
            // INIT SPARTAN
            String arg_home_directory = input(namespace.getString("home_directory"))
                    .asString()
                    .map(StringUtils::trimToEmpty)
                    .get();

            Spartan.init(arg_home_directory);

            // RUN
            RPCRestApi.getInstance().run();
        }
    }

    // STATIC
    private static RPCRestApi INSTANCE;

    public static String SOURCE_CONNECTION = "source";

    public static final int NORMAL_EXIT_CODE = 0;
    public static final int KILLED_EXIT_CODE = 1;
    public static final int BAD_ARGUMENTS_EXIT_CODE = 100;

    private static final Logger LOGGER = LoggerFactory.getLogger(RPCRestApi.class);

    static {
        INSTANCE = null;
    }

    public static RPCRestApi getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new RPCRestApi();
        }

        return INSTANCE;
    }

    // MEMBER
    public void run() {
        Javalin restapi = API.create();
	
        restapi.options("/*", ctx -> {
            ctx.header("Access-Control-Allow-Origin", "https://general-webpage-rpc-cms-rpc-general.app.cern.ch");  // Use your Angular app URL
            ctx.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
            ctx.header("Access-Control-Allow-Headers", "Content-Type, Authorization");
            ctx.header("Access-Control-Allow-Credentials", "true");
            ctx.status(200);  // Return 200 OK for OPTIONS request
        });

        // ENDPOINTS
        restapi.post("/lhc/log/blocks", new LogBlocksHandler());
        restapi.post("/cms/integrated-lumi", new IntegratedLuminosityHandler());
        restapi.post("/cms/integrated-lumi-total", new IntegratedLuminosityTotalHandler());
        restapi.post("/components", new ComponentsHandler());
        restapi.post("/components/chambers", new ChambersHandler());
        restapi.post("/hv/currents/power-on", new PowerOnHandler());
        restapi.post("/hv/currents/integrated-lumi", new IntegratedLuminosityHandler());
        restapi.post("/hv/currents/integrated-lumi-total", new IntegratedLuminosityTotalHandler());
        restapi.post("/hv/currents/integrated-charge", new IntegratedChargeHandler());
        restapi.post("/hv/currents/integrated-charge-total", new IntegratedChargeTotalHandler());
        restapi.post("/hv/currents/integrated-charge-per-year", new IntegratedChargePerYearHandler());
        restapi.post("/hv/currents/lumi-fit", new LumiFitHandler());
        restapi.post("/hv/currents/evolution", new EvolutionHandler());
        restapi.post("/hv/currents/cosmic", new CosmicHandler());
        restapi.post("/hv/currents/standby", new StandbyHandler());
        restapi.post("/hv/currents/hvcond", new HVCondHandler());
        restapi.post("/hv/currents/hvcond-fit", new HVCondFitHandler());
        restapi.post("/hv/currents/monitor", new CurrentsHandler());
        restapi.post("/hv/ml/lastnotifications", new MLNotificationsHandler());

        // GET PORT
        int port = input(cget("port"))
                .asInteger()
                .notNull()
                .get();

        // START
        restapi.start(port);
    }
}
