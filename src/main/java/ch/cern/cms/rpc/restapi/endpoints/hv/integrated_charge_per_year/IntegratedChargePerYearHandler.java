/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.cern.cms.rpc.restapi.endpoints.hv.integrated_charge_per_year;

import static ch.cern.cms.rpc.restapi.RPCRestApi.SOURCE_CONNECTION;
import ch.cern.cms.rpc.restapi.commons.components.Component;
import ch.cern.cms.rpc.restapi.commons.components.ComponentDPID;
import ch.cern.cms.rpc.restapi.commons.components.ComponentType;
import ch.cern.cms.rpc.restapi.commons.hv.IntegratedChargeType;
import ch.cern.cms.rpc.restapi.commons.hv.Normalizer;
import static ch.cern.cms.spartan.api.API.beginMessage;
import ch.cern.cms.spartan.api.APIHandler;
import ch.cern.cms.spartan.commons.database.connections.ConnectionShield;
import ch.cern.cms.spartan.commons.database.resultsets.ResultSetStreamTuple;
import ch.cern.cms.spartan.commons.threading.pool.Loan;
import ch.cern.cms.spartan.commons.time.Period;
import static ch.cern.cms.spartan.core.SpartanLang.connection;
import static ch.cern.cms.spartan.core.SpartanLang.list;
import static ch.cern.cms.spartan.core.SpartanLang.map;
import static ch.cern.cms.spartan.core.SpartanLang.statement;
import io.javalin.http.Context;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang3.time.DateUtils;

/**
 *
 * @author omiguelc
 */
public class IntegratedChargePerYearHandler extends APIHandler {

    @Override
    public void onRequest(Context ctx) {
        IntegratedChargePerYearRequest request = new IntegratedChargePerYearRequest(ctx);

        try {
            Map result = new HashMap();

            result.put("columns", list(
                    "FROM",
                    "TO",
                    "INTEGRATED_TIME",
                    "INTEGRATED_CHARGE"
            ));

            List<List> rows = new ArrayList();

            query(ctx, request, integrated_charge -> {
                rows.add(list(
                        integrated_charge.getFromInUnixtime(),
                        integrated_charge.getToInUnixtime(),
                        integrated_charge.getIntegratedTime(),
                        integrated_charge.getIntegratedCharge()
                ));
            });

            result.put("rows", rows);

            // RESPOND
            ctx.result(beginMessage()
                    .payload(result)
                    .end());
        } catch (SQLException ex) {
            Logger.getLogger(IntegratedChargePerYearHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void query(Context ctx, IntegratedChargePerYearRequest request, Consumer<IntegratedCharge> consumer) throws SQLException {
        // GET DPID LIST
        List<ComponentDPID> dpid_list = new Component(
                request.getParameters().getComponentName(),
                request.getParameters().getComponentType()
        ).getHVDPID();

        // PROCESS
        try (Loan<ConnectionShield> loan = connection(SOURCE_CONNECTION)) {
            List<Integrator> integrators = new ArrayList();

            SimpleDateFormat format = new SimpleDateFormat("yyyy");
            Calendar calendar_from = DateUtils.toCalendar(request.getParameters().getFrom());
            Calendar calendar_to = DateUtils.toCalendar(request.getParameters().getTo());
            int int_year_from = calendar_from.get(Calendar.YEAR);
            int int_year_to = calendar_to.get(Calendar.YEAR);

            for (int year = int_year_from; year < int_year_to; year++) {
                try {
                    Date year_from = format.parse(Integer.toString(year));
                    Date year_to = format.parse(Integer.toString(year + 1));
                    integrators.add(new Integrator(year_from, year_to));
                } catch (ParseException ex) {
                    // WILL NEVER HAPPEN
                }
            }

            // GET NORMALIZERS
            String normalizer_source;

            if (ComponentType.VIRTUAL_CHAMBER.equals(request.getParameters().getComponentType())) {
                normalizer_source = "hv::selectVirtualNormalizers";
            } else {
                normalizer_source = "hv::selectNormalizers";
            }

            statement(normalizer_source)
                    .queryAndGetMaps(
                            loan.value(),
                            map("COMPONENT_NAME", request.getParameters().getComponentName())
                    )
                    .stream()
                    .map(Normalizer::new)
                    .forEach(normalizer -> {
                        integrators.forEach(integrator -> {
                            integrator.addNormalizer(normalizer);
                        });
                    });

            // RUN IC STATEMENT
            for (ComponentDPID dpid : dpid_list) {
                // FIND OVERLAP
                Period overlap = dpid.getPeriod().getOverlap(request.getParameters().getPeriod());

                // QUERY
                if (overlap != null) {
                    // REQUEST
                    Map query_args = map(
                            "DPID", dpid.getDPID(),
                            "FROM", overlap.getStartingDate(),
                            "TO", overlap.getEndingDate(),
                            "IC", request.getParameters().getICType()
                    );

                    // RUN STATEMENT
                    String ic_source;

                    if (IntegratedChargeType.COLLISION_NO_COSMIC.equals(request.getParameters().getICType())) {
                        ic_source = "hv::selectIntegratedChargeCNC";
                    } else {
                        ic_source = "hv::selectIntegratedCharge";
                    }

                    ResultSetStreamTuple<Map> ic_stream_tuple = statement(ic_source)
                            .queryAndStreamMaps(loan.value(), query_args);

                    // INTEGRATE
                    ic_stream_tuple.getStream()
                            .map(IntegratedCharge::new)
                            .forEach(integrated_charge -> {
                                integrators.forEach(integrator -> {
                                    integrator.process(integrated_charge);
                                });
                            });

                    // CHECK STREAM IS OK
                    ic_stream_tuple.getExceptionPointer().throwIfPresent();
                }
            }

            // FOR EACH
            integrators.forEach(integrator -> {
                consumer.accept(new IntegratedCharge(map(
                        "FROM", integrator.getFrom(),
                        "TO", integrator.getTo(),
                        "INTEGRATED_TIME", integrator.getIntegratedTime(),
                        "INTEGRATED_CHARGE", integrator.getIntegratedChargeDensity() * 1e-3
                )));
            });
        }
    }

    // CLASS
    private static class Integrator {

        private final Period period;
        private final Date from;
        private final Date to;

        private long integrated_time;
        private double integrated_charge;
        private double integrated_charge_density;

        private Normalizer normalizer;
        private final List<Normalizer> normalizers;

        public Integrator(Date from, Date to) {
            this.period = new Period(from, to);
            this.from = from;
            this.to = to;

            this.integrated_time = 0;
            this.integrated_charge = 0;
            this.integrated_charge_density = 0;
            this.normalizers = new ArrayList();
        }

        public Date getFrom() {
            return from;
        }

        public Date getTo() {
            return to;
        }

        public void addNormalizer(Normalizer normalizer) {
            if (this.normalizer == null) {
                this.normalizer = normalizer;
            } else {
                normalizers.add(normalizer);
            }
        }

        public void process(IntegratedCharge record) {
            // CHECK
            if (!period.contains(record.getFrom())) {
                return;
            }

            // NORMALIZE
            if (!normalizer.validFor(record.getFrom())) {
                // REMEMBER CURRENT NORMALIZER
                Normalizer old_normalizer = normalizer;

                // SAVE CURRENT DENSITY
                double old_density = (integrated_charge / old_normalizer.getArea());

                // FIND NORMALIZER
                for (Normalizer candidate : normalizers) {
                    if (candidate.validFor(record.getFrom())) {
                        normalizer = candidate;
                        break;
                    }
                }

                // REMOVE FROM NORMALIZERS
                normalizers.remove(normalizer);

                // RENORMALIZE INTEGRATED CHARGE
                if (normalizer != null) {
                    this.integrated_charge = normalizer.getArea() * old_density;
                }
            }

            // INTEGRATE
            if (normalizer != null) {
                this.integrated_time += record.getIntegratedTime();
                this.integrated_charge += record.getIntegratedCharge()
                        * normalizer.getRatio();
                this.integrated_charge_density = integrated_charge / normalizer.getArea();
            }
        }

        public Normalizer getCurrentNormalizer() {
            return normalizer;
        }

        public long getIntegratedTime() {
            return integrated_time;
        }

        public double getIntegratedCharge() {
            return integrated_charge;
        }

        public double getIntegratedChargeDensity() {
            return integrated_charge_density;
        }
    }

    private static class IntegratedCharge {

        private final Date from;
        private final Date to;

        private final Period period;

        private final long integrated_time;
        private final double integrated_charge;

        public IntegratedCharge(Map map) {
            this.from = (Date) map.get("FROM");
            this.to = (Date) map.get("TO");

            this.period = new Period(from, to);

            this.integrated_time = (long) map.get("INTEGRATED_TIME");
            this.integrated_charge = (double) map.get("INTEGRATED_CHARGE");
        }

        // GETTERS
        public Date getFrom() {
            return from;
        }

        public Date getTo() {
            return to;
        }

        public long getFromInUnixtime() {
            return from.getTime();
        }

        public long getToInUnixtime() {
            return to.getTime();
        }

        public Period getPeriod() {
            return period;
        }

        public long getIntegratedTime() {
            return integrated_time;
        }

        public double getIntegratedCharge() {
            return integrated_charge;
        }
    }
}
