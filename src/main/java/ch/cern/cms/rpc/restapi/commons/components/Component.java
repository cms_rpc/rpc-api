/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.cern.cms.rpc.restapi.commons.components;

import static ch.cern.cms.rpc.restapi.RPCRestApi.SOURCE_CONNECTION;
import ch.cern.cms.rpc.restapi.commons.dpids.DPIDType;
import ch.cern.cms.spartan.commons.database.connections.ConnectionShield;
import ch.cern.cms.spartan.commons.database.resultsets.ResultSetStreamTuple;
import ch.cern.cms.spartan.commons.threading.pool.Loan;
import static ch.cern.cms.spartan.core.SpartanLang.connection;
import static ch.cern.cms.spartan.core.SpartanLang.map;
import static ch.cern.cms.spartan.core.SpartanLang.statement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author omiguelc
 */
public class Component {

    // MEMBER
    private final String component_name;
    private final ComponentType component_type;

    public Component(String component_name, ComponentType component_type) {
        this.component_name = component_name;
        this.component_type = component_type;
    }

    public List<ComponentDPID> getHVDPID() throws SQLException {
        DPIDType dpid_type;

        switch (component_type) {
            case VIRTUAL_CHAMBER:
                dpid_type = DPIDType.VHV;
                break;
            case CHAMBER:
                dpid_type = DPIDType.HV;
                break;
            default:
                throw new IllegalArgumentException();
        }

        // RETURN
        return getDPID(dpid_type);
    }

    public List<ComponentDPID> getDPID(DPIDType dpid_type) throws SQLException {
        try (Loan<ConnectionShield> loan = connection(SOURCE_CONNECTION)) {
            ResultSetStreamTuple<Map> stream_tuple = statement("components::selectDPID")
                    .queryAndStreamMaps(
                            loan.value(),
                            map(
                                    "COMPONENT_NAME", component_name,
                                    "COMPONENT_TYPE", component_type,
                                    "DPID_TYPE", dpid_type.toString()
                            )
                    );

            // BUFFER
            List<ComponentDPID> dpid_list = new ArrayList();

            // RETRIEVE
            stream_tuple.getStream()
                    .map(ComponentDPID::new)
                    .forEach(dpid_list::add);

            // CHECK STREAM IS OK
            stream_tuple.getExceptionPointer().throwIfPresent();

            // RETURN
            return dpid_list;
        }
    }
}
