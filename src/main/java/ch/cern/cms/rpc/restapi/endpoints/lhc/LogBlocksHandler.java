/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.cern.cms.rpc.restapi.endpoints.lhc;

import static ch.cern.cms.rpc.restapi.RPCRestApi.SOURCE_CONNECTION;
import static ch.cern.cms.spartan.api.API.beginMessage;
import ch.cern.cms.spartan.api.APIHandler;
import ch.cern.cms.spartan.commons.database.connections.ConnectionShield;
import ch.cern.cms.spartan.commons.database.resultsets.ResultSetStreamTuple;
import ch.cern.cms.spartan.commons.threading.pool.Loan;
import static ch.cern.cms.spartan.core.SpartanLang.connection;
import static ch.cern.cms.spartan.core.SpartanLang.list;
import static ch.cern.cms.spartan.core.SpartanLang.map;
import static ch.cern.cms.spartan.core.SpartanLang.statement;
import io.javalin.http.Context;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author omiguelc
 */
public class LogBlocksHandler extends APIHandler {

    @Override
    public void onRequest(Context ctx) {
        LogBlocksRequest request = new LogBlocksRequest(ctx);

        try {
            Map result = new HashMap();

            result.put("columns", list(
                    "STARTTIME",
                    "STOPTIME",
                    "START_REASON",
                    "STOP_REASON",
                    "MACHINE_MODE",
                    "TYPE"
            ));

            List<List> rows = new ArrayList();

            query(ctx, request, block -> {
                rows.add(list(
                        block.getUnixStartTime(),
                        block.getUnixStopTime(),
                        block.getStartReason(),
                        block.getStopReason(),
                        block.getMachineMode(),
                        block.getType()
                ));
            });

            result.put("rows", rows);

            // RESPOND
            ctx.result(beginMessage()
                    .payload(result)
                    .end());
        } catch (SQLException ex) {
            Logger.getLogger(LogBlocksHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void query(Context ctx, LogBlocksRequest request, Consumer<LHCLogBlock> consumer) throws SQLException {
        Map query_args = map(
                "FROM", request.getParameters().getFrom(),
                "TO", request.getParameters().getTo()
        );

        try (Loan<ConnectionShield> loan = connection(SOURCE_CONNECTION)) {
            // RUN STATEMENT
            ResultSetStreamTuple<Map> stream_tuple = statement("lhc::selectLogBlocks")
                    .queryAndStreamMaps(loan.value(), query_args);

            // FOR EACH
            stream_tuple.getStream()
                    .map(LHCLogBlock::new)
                    .forEach(consumer);

            // CHECK STREAM IS OK
            stream_tuple.getExceptionPointer().throwIfPresent();
        }
    }

    // CLASS
    private static class LHCLogBlock {

        private final Date starttime;
        private final Date stoptime;
        private final String start_reason;
        private final String stop_reason;
        private final String machine_mode;
        private final String type;

        public LHCLogBlock(Map record) {
            starttime = (Date) record.get("STARTTIME");
            stoptime = (Date) record.get("STOPTIME");
            start_reason = (String) record.get("START_REASON");
            stop_reason = (String) record.get("STOP_REASON");
            machine_mode = (String) record.get("MACHINE_MODE");
            type = (String) record.get("TYPE");
        }

        public Date getStartTime() {
            return starttime;
        }

        public Date getStopTime() {
            return stoptime;
        }

        public long getUnixStartTime() {
            return starttime.getTime();
        }

        public long getUnixStopTime() {
            return stoptime.getTime();
        }

        public String getStartReason() {
            return start_reason;
        }

        public String getStopReason() {
            return stop_reason;
        }

        public String getMachineMode() {
            return machine_mode;
        }

        public String getType() {
            return type;
        }
    }
}
