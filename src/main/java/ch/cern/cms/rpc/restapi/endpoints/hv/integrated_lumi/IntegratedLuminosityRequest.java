/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.cern.cms.rpc.restapi.endpoints.hv.integrated_lumi;

import ch.cern.cms.rpc.restapi.commons.components.ComponentType;
import ch.cern.cms.spartan.api.APIMessage;
import ch.cern.cms.spartan.commons.time.Period;
import static ch.cern.cms.spartan.core.SpartanLang.input;
import io.javalin.http.Context;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import org.apache.commons.lang3.EnumUtils;

/**
 *
 * @author omiguelc
 */
public class IntegratedLuminosityRequest {

    private final Parameters parameters;

    public IntegratedLuminosityRequest(Context ctx) {
        APIMessage request = new APIMessage(ctx.body());

        // READ
        this.parameters = new Parameters((Map) request.payload("parameters"));
    }

    public Parameters getParameters() {
        return parameters;
    }

    public class Parameters {

        private final ComponentType component_type;
        private final String component_name;
        private final Date from;
        private final Date to;

        public Parameters(Map parameters) {
            this.component_type = input(parameters.get("component_type"))
                    .asString()
                    .notNull()
                    .check(input -> EnumUtils.isValidEnumIgnoreCase(ComponentType.class, input), "Invalid component_type.")
                    .map(input -> EnumUtils.getEnumIgnoreCase(ComponentType.class, input))
                    .get();

            this.component_name = input(parameters.get("component_name"))
                    .asString()
                    .notNull()
                    .get();

            this.from = input(parameters.get("from"))
                    .asDate()
                    .notNull()
                    .get();

            this.to = input(parameters.get("to"))
                    .asDate()
                    .ifNull(Calendar.getInstance().getTime())
                    .get();
        }

        public ComponentType getComponentType() {
            return component_type;
        }

        public String getComponentName() {
            return component_name;
        }

        public Date getFrom() {
            return from;
        }

        public Date getTo() {
            return to;
        }

        public Period getPeriod() {
            return new Period(from, to);
        }
    }

}
