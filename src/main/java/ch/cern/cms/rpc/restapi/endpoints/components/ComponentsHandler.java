/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.cern.cms.rpc.restapi.endpoints.components;

import static ch.cern.cms.rpc.restapi.RPCRestApi.SOURCE_CONNECTION;
import static ch.cern.cms.spartan.api.API.beginMessage;
import ch.cern.cms.spartan.api.APIHandler;
import ch.cern.cms.spartan.commons.database.connections.ConnectionShield;
import ch.cern.cms.spartan.commons.database.resultsets.ResultSetStreamTuple;
import ch.cern.cms.spartan.commons.threading.pool.Loan;
import static ch.cern.cms.spartan.core.SpartanLang.connection;
import static ch.cern.cms.spartan.core.SpartanLang.list;
import static ch.cern.cms.spartan.core.SpartanLang.map;
import static ch.cern.cms.spartan.core.SpartanLang.statement;
import io.javalin.http.Context;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author omiguelc
 */
public class ComponentsHandler extends APIHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(ComponentsHandler.class);

    @Override
    public void onRequest(Context ctx) {
        ComponentsRequest request = new ComponentsRequest(ctx);
        System.out.println("components request...");
        try {
            Map result = new HashMap();

            result.put("columns", list(
                    "NAME",
                    "DESCRIPTION"
            ));

            List<List> rows = new ArrayList();

            query(ctx, request, record -> {
                rows.add(list(
                        record.getName(),
                        record.getDescription()
                ));
            });

            result.put("rows", rows);

            // RESPOND
            ctx.result(beginMessage()
                    .payload(result)
                    .end());
        } catch (SQLException ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
    }

    private void query(Context ctx, ComponentsRequest request, Consumer<Record> consumer) throws SQLException {
        try (Loan<ConnectionShield> loan = connection(SOURCE_CONNECTION)) {
            // RUN STATEMENT
            ResultSetStreamTuple<Map> stream_tuple = statement("components::getAll")
                    .queryAndStreamMaps(
                            loan.value(),
                            map("TYPE", request.getParameters().getType())
                    );

            // FOR EACH
            stream_tuple.getStream()
                    .map(Record::new)
                    .forEach(consumer);

            // CHECK STREAM IS OK
            stream_tuple.getExceptionPointer().throwIfPresent();
        }
    }

    // CLASS
    private static class Record {

        private final String name;
        private final String description;

        public Record(Map record) {
            this.name = (String) record.get("NAME");
            this.description = (String) record.get("DESCRIPTION");
        }

        public String getName() {
            return name;
        }

        public String getDescription() {
            return description;
        }

    }
}
