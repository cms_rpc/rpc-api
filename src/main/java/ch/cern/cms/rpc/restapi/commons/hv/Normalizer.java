/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.cern.cms.rpc.restapi.commons.hv;

import ch.cern.cms.spartan.commons.time.Period;
import java.util.Date;
import java.util.Map;

/**
 *
 * @author omiguelc
 */
public class Normalizer {

    private final Date from;
    private final Date to;

    private final Period period;

    private final double area;
    private final double ratio;

    public Normalizer(Map map) {
        this.from = (Date) map.get("FROM");
        this.to = (Date) map.get("TO");

        this.period = new Period(from, to);

        this.area = (double) map.get("CALC_GAP_AREA");
        this.ratio = (double) map.get("CALC_GAP_AREA_RATIO");
    }

    // SPECIAL
    public boolean validFor(Date date) {
        return period.contains(date);
    }

    // GETTERS
    public Date getFrom() {
        return from;
    }

    public Date getTo() {
        return to;
    }

    public long getFromInUnixtime() {
        return from.getTime();
    }

    public long getToInUnixtime() {
        return to.getTime();
    }

    public Period getPeriod() {
        return period;
    }

    public double getArea() {
        return area;
    }

    public double getRatio() {
        return ratio;
    }
}
