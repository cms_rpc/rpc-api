/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.cern.cms.rpc.restapi.endpoints.hv.currents;

import static ch.cern.cms.rpc.restapi.RPCRestApi.SOURCE_CONNECTION;
import ch.cern.cms.rpc.restapi.commons.components.Component;
import ch.cern.cms.rpc.restapi.commons.components.ComponentDPID;
import ch.cern.cms.rpc.restapi.endpoints.hv.evolution.EvolutionHandler;
import static ch.cern.cms.spartan.api.API.beginMessage;
import ch.cern.cms.spartan.api.APIHandler;
import ch.cern.cms.spartan.commons.database.connections.ConnectionShield;
import ch.cern.cms.spartan.commons.database.resultsets.ResultSetStreamTuple;
import ch.cern.cms.spartan.commons.database.statements.StatementShield;
import ch.cern.cms.spartan.commons.database.statements.StatementTemplate;
import ch.cern.cms.spartan.commons.threading.pool.Loan;
import ch.cern.cms.spartan.commons.time.Period;
import static ch.cern.cms.spartan.core.SpartanLang.connection;
import static ch.cern.cms.spartan.core.SpartanLang.list;
import static ch.cern.cms.spartan.core.SpartanLang.map;
import static ch.cern.cms.spartan.core.SpartanLang.sql;
import static ch.cern.cms.spartan.core.SpartanLang.template;
import io.javalin.http.Context;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author omiguelc
 */
public class CurrentsHandler extends APIHandler {

    @Override
    public void onRequest(Context ctx) {
        CurrentsRequest request = new CurrentsRequest(ctx);

        try {
            Map result = new HashMap();

            result.put("columns", list(
                    "STARTTIME",
                    "STOPTIME",
                    "VMON",
                    "IMON",
                    "STATUS",
                    "FLAG"
            ));

            List<List> rows = new ArrayList();

            query(ctx, request, current -> {
                rows.add(list(
                        current.getUnixStarttime(),
                        current.getUnixStoptime(),
                        current.getVmon(),
                        current.getImon(),
                        current.getStatus(),
                        current.getFlag()
                ));
            });

            result.put("rows", rows);

            // RESPOND
            ctx.result(beginMessage()
                    .payload(result)
                    .end());
        } catch (SQLException ex) {
            Logger.getLogger(EvolutionHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void query(Context ctx, CurrentsRequest request, Consumer<Current> consumer) throws SQLException {
        // GET DPID LIST
        List<ComponentDPID> dpid_list = new Component(
                request.getParameters().getComponentName(),
                request.getParameters().getComponentType()
        ).getHVDPID();

        // FIND SOURCE
        StatementTemplate template = template("hv::selectCurrents");

        if (request.getParameters().getPeriod().getDurationSeconds() < 86400) {
            template.define("dynamic_table", sql("{% rpccurrents %}"));
        } else {
            template.define("dynamic_table", sql("{% rpccurrents_moving_average %}"));
        }

        StatementShield statement = template.statement();

        // PROCESS
        for (ComponentDPID dpid : dpid_list) {
            // FIND OVERLAP
            Period overlap = dpid.getPeriod().getOverlap(request.getParameters().getPeriod());

            // QUERY
            if (overlap != null) {
                // REQUEST
                Map query_args = map(
                        "DPID", dpid.getDPID(),
                        "FROM", overlap.getStartingDate(),
                        "TO", overlap.getEndingDate()
                );

                try (Loan<ConnectionShield> loan = connection(SOURCE_CONNECTION)) {
                    // RUN STATEMENT
                    ResultSetStreamTuple<Map> stream_tuple = statement.queryAndStreamMaps(
                            loan.value(), query_args
                    );

                    // FOR EACH
                    Map record = new HashMap();

                    stream_tuple.getStream()
                            .map(next_record -> {
                                Map new_record = null;

                                if (!record.isEmpty()) {
                                    new_record = new HashMap(record);
                                    new_record.put("STARTTIME", record.get("CHANGE_DATE"));
                                    new_record.put("STOPTIME", next_record.get("CHANGE_DATE"));
                                    new_record.remove("CHANGE_DATE");
                                }

                                record.clear();
                                record.putAll(next_record);

                                return new_record;
                            })
                            // REMOVE NULLS
                            .filter(Objects::nonNull)
                            .map(Current::new)
                            .forEach(consumer);

                    // CHECK STREAM IS OK
                    stream_tuple.getExceptionPointer().throwIfPresent();
                }
            }
        }
    }

    // CLASS
    private static class Current {

        private final Date starttime;
        private final Date stoptime;
        private final int vmon;
        private final double imon;
        private final int status;
        private final int flag;

        public Current(Map record) {
            starttime = (Date) record.get("STARTTIME");
            stoptime = (Date) record.get("STOPTIME");
            vmon = (int) record.get("VMON");
            imon = (double) record.get("IMON");
            status = (int) record.get("STATUS");
            flag = (int) record.get("FLAG");
        }

        public Date getStarttime() {
            return starttime;
        }

        public Date getStoptime() {
            return stoptime;
        }

        public long getUnixStarttime() {
            return starttime.getTime();
        }

        public long getUnixStoptime() {
            return stoptime.getTime();
        }

        public int getVmon() {
            return vmon;
        }

        public double getImon() {
            return imon;
        }

        public int getStatus() {
            return status;
        }

        public int getFlag() {
            return flag;
        }
    }
}
