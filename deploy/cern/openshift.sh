# Set the user and token variables with the values provided by GitLab on creation of the Deploy Token.
user=''
token=''
# Generate the secret (make sure to `oc login` into your Openshift project first)
auth=$(echo -n "${user}:${token}" | base64 -w 0)
dockercfg=$(echo "{\"auths\": {\"gitlab-registry.cern.ch\": {\"auth\": \"${auth}\"}, \"gitlab.cern.ch\": {\"auth\": \"${auth}\"}}}")
oc create secret generic gitlab-registry-auth --from-literal=.dockerconfigjson="${dockercfg}" --type=kubernetes.io/dockerconfigjson
# After this, assign the registry pull secret to the service account running your application.
# Please note that, by default, applications are run by the 'default' service account.
# If that is not your case, adjust the service account name in the command as appropriate:
oc secrets link default gitlab-registry-auth --for=pull
