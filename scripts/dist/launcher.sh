#!/usr/bin/env sh

# GET PROJECT PATH
project_path="$( cd "$(dirname "$0")/../" ; pwd -P )"

# RUN
source "$project_path/bin/rpc-restapi" -d "$project_path/etc" "$@"
