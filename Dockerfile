# ENVIRONMENT
FROM cern/alma8-base

RUN yum -y update
RUN yum -y install tar which java-1.8.0-openjdk-devel

# BUILD
RUN mkdir -p /usr/src/app
COPY . /usr/src/app
WORKDIR /usr/src/app
RUN source ./.docker/java_home.sh
RUN ./gradlew clean build

# INSTALL
RUN mkdir -p /usr/share
WORKDIR /usr/share
RUN tar -xvf /usr/src/app/build/distributions/rpc-restapi-prod.tar

# EXPOSE
EXPOSE 9610

RUN chmod +x /usr/share/rpc-restapi-prod/bin/launcher.sh

# ENTRYPOINT
CMD ["/usr/share/rpc-restapi-prod/bin/launcher.sh"]
